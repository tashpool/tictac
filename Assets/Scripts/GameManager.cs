using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    [SerializeField] float spawnRate = 1.0f;
    
    public List<GameObject> targets;
    public GameObject winTarget;
    public GameObject skullHint;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI gameOverText;
    public TextMeshProUGUI winText;
    public bool isGameActive;
    public Button restartButton;
    
    private int _score;
    private int _winPos;

    private readonly Vector3[] _spawnPos =
    {
        new Vector3(-4.0f, 6.5f, -4.0f),
        new Vector3(-1.5f, 6.5f, -4.0f),
        new Vector3(1.0f, 6.5f, -4.0f),
        new Vector3(3.5f, 6.5f, -4.0f),
        new Vector3(-4.0f, 4.0f, -4.0f),
        new Vector3(-1.5f, 4.0f, -4.0f),
        new Vector3(1.0f, 4.0f, -4.0f),
        new Vector3(3.5f, 4.0f, -4.0f),
        new Vector3(-4.0f, 1.5f, -4.0f),
        new Vector3(-1.5f, 1.5f, -4.0f),
        new Vector3(1.0f, 1.5f, -4.0f),
        new Vector3(3.5f, 1.4f, -4.0f),
        new Vector3(-4.0f, -1.0f, -4.0f),
        new Vector3(-1.5f, -1.0f, -4.0f),
        new Vector3(1.0f, -1.0f, -4.0f),
        new Vector3(3.5f, -1.0f, -4.0f),
    };
    
    // Start is called before the first frame update
    void Start()
    {
        isGameActive = true;
        UpdateScore(50);
        _winPos = Random.Range(1, _spawnPos.Length);
        StartCoroutine(SpawnCrate());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SpawnCrate()
    {
        int spawnIdx = 1;
        foreach (var qPos in _spawnPos)
        {
            yield return new WaitForSeconds(spawnRate);
            GameObject crate = Instantiate(targets[0], qPos, Quaternion.identity);
            // Debug.Log(spawnIdx.ToString() + " : " + _winPos.ToString());
            if (spawnIdx == _winPos)
            {
                crate.tag = "Good";
            }
            spawnIdx++;
        }
    }

    public void UpdateScore(int scoreToAdd)
    {
        _score += scoreToAdd;
        scoreText.text = "Score: " + _score;
        if (_score <= 0)
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        isGameActive = false;
        gameOverText.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(true);
    }

    public void WonGame()
    {
        isGameActive = false;
        Instantiate(winTarget, _spawnPos[_winPos-1], Quaternion.identity);
        winText.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void setSkullHint(Transform crateTransform)
    {
        var skull = Instantiate(skullHint, crateTransform.position, Quaternion.identity);
        GameObject goal = GameObject.FindWithTag("Good");
        if (goal != null)
        {
            // var goalVector = new Quaternion(goal.transform.position.x, goal.transform.position.y, goal.transform.position.z, 1);
            
            // If it's purely UP, can't see the eyes when close to goal
            Vector3 skullUp = new Vector3(0.0f, 0.75f, -1.0f);
            // Vector3.back
            skull.transform.LookAt(goal.transform.position, Vector3.Normalize(skullUp));
        }
        else
        {
            skull.transform.LookAt(Camera.main.transform);
            Debug.Log("ERROR: No goal set!");
        }
    }
}
