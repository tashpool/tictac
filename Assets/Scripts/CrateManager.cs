using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using Random = UnityEngine.Random;

public class CrateManager : MonoBehaviour
{
    // private Rigidbody _createRigidbody;
    [SerializeField] private float maxTorque = 10;
    [SerializeField] private float accelx = 20;
    [SerializeField] private float accely = 20;
    [SerializeField] private float accelz = 20;

    private GameManager _gameManager;

    public ParticleSystem explosionParticle;
    
    // Start is called before the first frame update
    void Start()
    {
        _gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
    }

    float RandomTorque()
    {
        return Random.Range(-maxTorque, maxTorque);
    }

    float RandomAngle()
    {
        return Random.Range(-360, 360);
    }

    // Update is called once per frame
    void Update()
    {
        // transform.Rotate(RandomAngle(), RandomAngle(), RandomAngle());
        transform.Rotate(accelx * Time.deltaTime, accely * Time.deltaTime, accelz * Time.deltaTime);
    }

    private void OnMouseDown()
    {
        if (!_gameManager.isGameActive) return;
        
        Destroy(gameObject);
        Instantiate(explosionParticle, transform.position, explosionParticle.transform.rotation);

        if (gameObject.CompareTag("Good"))
        {
            _gameManager.WonGame();
            return;
            // spawn winning item / effect
        }
        
        _gameManager.UpdateScore(-10);
        _gameManager.setSkullHint(transform);
    }
}
