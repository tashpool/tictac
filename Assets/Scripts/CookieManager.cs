using UnityEngine;

public class CookieManager : MonoBehaviour
{
    [SerializeField] private float maxTorque = 10;
    [SerializeField] private float accelx = 20;
    [SerializeField] private float accely = 20;
    [SerializeField] private float accelz = 20;
    
    float RandomTorque()
    {
        return Random.Range(-maxTorque, maxTorque);
    }

    float RandomAngle()
    {
        return Random.Range(-360, 360);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(accelx * Time.deltaTime, accely * Time.deltaTime, accelz * Time.deltaTime);
    }
}